# README #

This project is used predict the gross collections of a movie using linear regression algorithm with various features.
The data has been scraped from boxofficeindia,wikipedia and youtube.
The model has an accuracy of ~ 80% on test data.
Libraries Used:sklearn,numpy,pandas,Beautiful Soup etc.

